                 !!!!!!!!!!!!!!!!!!!!!!!!!
                    MODULE FORMFACTORS3L
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  implicit none
  integer, parameter :: prec = selected_real_kind(15,32)
  integer, parameter :: order = 5
  integer, parameter :: ninterpol = 3
  real(kind=prec), parameter :: ibounds(0:ninterpol) = (/-41.00001, 4., 16., 60.99999/)

  TYPE INTERPOLATION
    integer nd
    real(kind=prec), allocatable :: x(:)
    complex(kind=prec), allocatable :: table(:, :)
  END TYPE INTERPOLATION

  TYPE INTERPOLATIONS
    type(interpolation) :: interpol(ninterpol)
  END TYPE INTERPOLATIONS

  INTERFACE EVALUATE
    module procedure evaluate1, evaluaten
  END INTERFACE EVALUATE

contains

  ! Calculates the differences of n neighbours
  FUNCTION DIFFERENCE(arr, n)
  implicit none
  complex(kind=prec), intent(in) :: arr(:)
  integer, intent(in) :: n
  complex(kind=prec) :: difference(size(arr)-n)
  integer i

  do i=1,size(arr)-n
    difference(i) = arr(i+n) - arr(i)
  enddo
  END FUNCTION DIFFERENCE

  ! Let the interpolating polynomial be of the form
  !
  !  d[0] + (x-x1)(d[1] + (x-x2)(d[2] + (x-x3)d3 ))
  !
  ! This functions builds the d
  FUNCTION INTERPOLATE1(x, y)
  implicit none
  real(kind=prec), intent(in) :: x(order+1)
  complex(kind=prec), intent(in) :: y(order+1)
  complex(kind=prec) :: interpolate1(order+1)
  complex(kind=prec) :: diff(order+1)
  integer n

  diff = y
  do n=1,order+1
    interpolate1(n) = diff(1)
    diff = difference(diff, 1)/difference(cmplx(x, kind=prec), n)
  enddo

  END FUNCTION INTERPOLATE1

  FUNCTION INTERPOLATE(x, y, nd)
  implicit none
  integer, intent(in) :: nd
  real(kind=prec), intent(in) :: x(nd)
  complex(kind=prec), intent(in) :: y(nd)
  type(interpolation) :: interpolate
  integer i

  allocate(interpolate%x(nd))
  allocate(interpolate%table(nd-order+1, order+1))

  interpolate%x = x
  do i=1, nd-order+1
    interpolate%table(i,:) = interpolate1(x(i:i+order+1), y(i:i+order+1))
  enddo

  END FUNCTION INTERPOLATE

  FUNCTION EVALUATE1(interpol, x)
  real(kind=prec), intent(in) :: x(:)
  type(interpolation), intent(in) :: interpol
  complex(kind=prec) :: evaluate1(size(x))
  complex(kind=prec) :: tab(size(x), 0:order)
  real(kind=prec) :: xx(size(x), 0:order)
  integer ind(size(x))
  integer i

  do i=1, size(x)
    ind(i) = findloc(interpol%x - x(i) < 0, .false., 1)
    if(ind(i) == 0) then
      ind(i) = size(interpol%x)
    endif

    ind(i) = ind(i) - order/2 - 1
    if(ind(i) < 1) ind(i) = 1
    if(ind(i) > size(interpol%table,1)) ind(i) = size(interpol%table,1)

    xx(i, :) = interpol%x(ind(i):ind(i)+order+1)
  enddo

  tab = interpol%table(ind, :)

  evaluate1 = 0.
  do i=order,1,-1
    evaluate1 = (evaluate1 + tab(:, i))*(x - xx(:, i-1))
  enddo
  evaluate1 = evaluate1 + tab(:, 0)

  END FUNCTION EVALUATE1


  FUNCTION EVALUATEN(interpols, x)
    use, intrinsic :: iso_fortran_env
    use, intrinsic :: ieee_arithmetic
  real(kind=prec), intent(in) :: x(:)
  type(interpolations), intent(in) :: interpols
  complex(kind=prec) :: evaluaten(size(x))
  integer i

  evaluaten = 0.
  evaluaten = 1/evaluaten

  do i=1,ninterpol
    where ( (ibounds(i-1) < x).and.(x < ibounds(i)) ) &
        evaluaten = evaluate(interpols%interpol(i), x)
  enddo

  END FUNCTION EVALUATEN


  FUNCTION READ_FILE(filename, u)
  implicit none
  character(len=*), intent(in), optional :: filename
  integer, intent(in), optional :: u
  type(interpolations) :: read_file
  integer uu

  integer nxspec, n, i, j, mi, ma
  real(kind=prec), allocatable :: x0d(:,:)
  integer, allocatable :: ns(:)

  real(kind=prec), allocatable :: dat(:,:), x(:)
  complex(kind=prec), allocatable :: y(:)

  if(present(filename).and..not.present(u)) then
    uu = 8
    open(                             &
        unit=uu,                      &
        action='READ',                &
        file=trim(adjustl(filename)), &
        form='UNFORMATTED'            &
    )
  elseif(.not.present(filename).and.present(u)) then
    uu = u
  endif

  read(uu) nxspec
  allocate(x0d(2, nxspec))
  allocate(ns(nxspec))
  read(uu) x0d
  read(uu) ns

  n = sum(ns+1)

  allocate(x(0:n-1))
  allocate(y(0:n-1))
  allocate(dat(2, n))

  read(uu) dat
  y = dat(1,:) + dat(2,:) * (0._prec, 1._prec)

  n = 0
  do i=1,nxspec
    do j=0, ns(i)
      x(n+j) = x0d(1, i) + j * x0d(2, i)
    enddo
    n = n + ns(i) + 1
  enddo

  do i=1,ninterpol
    mi = findloc(ibounds(i-1) < x, .true., 1)
    ma = findloc(ibounds(i) > x, .true., 1, back=.true.)
    read_file%interpol(i) = interpolate(x(mi:ma), y(mi:ma), ma-mi)
  enddo

  if(present(filename).and..not.present(u)) then
    close(uu)
  endif
  END FUNCTION READ_FILE

                 !!!!!!!!!!!!!!!!!!!!!!!!!
                 END MODULE FORMFACTORS3L
                 !!!!!!!!!!!!!!!!!!!!!!!!!

