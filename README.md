# FormFactors3l

FormFactors3l is a Mathematica package that evaluates the third-order corrections to the QCD form factors.
It uses interpolated grids for the evaluation of the form factors in the range *-40 < s/m^2 < 3.75*
and *4.25 < s/m^2 < 60*. In the remaining regions, it uses special series expansion around *s=+-Infinity* and *s=4*.

It is based on the results presented in:

M. Fael, F. Lange, K. Schönwald, M. Steinhauser     
Massive Vector Form Factors to Three Loops     
Phys.Rev.Lett. 128 (2022) 17, 172003     
arXiv: 2202.05276 [hep-ph]    

M. Fael, F. Lange, K. Schönwald, M. Steinhauser     
Singlet and non-singlet three-loop massive form factors     
arXiv: 2207.00027 [hep-ph]  

## Getting started

The package can be loaded in a Mathematica session with the command
```
Get["PATH/FormFactors3l.m"]
```
provided the `PATH` points to the directory of the package. All
functionality is contained in this file, there are no dependencies other than Mathematica itself.
The package has been tested on a Linux operating system with the Mathematica versions 8 to 13.

## Bare form factors at three loops

The functions `FormFactorBareNonSing[CurrentType, EpsilonOrder, s]` and  `FormFactorBareNhSing[CurrentType, EpsilonOrder, s]`
return the non-singlet and nh-singlet contributions to the bare form factors at three loops, respectively.
The result is the third-order correction in the expansion parameter &alpha;<sub>s</sub><sup>bare</sup>/&pi;.
The various form factors are denoted by `CurrentType = {veF1, veF2, axF1, axF2, scF1, psF1}`.
The order in the dimensional regulator &epsilon; = (4-d)/2 is denoted by `EpsilonOrder`.

Example:
```
In[]  := FormFactorBareNonSing[veF1, 0, -1]
Out[] := 77.05064956158255*cA^2*cR + 95.06338906946412*cA*cR^2 + 0.46746584951173675*cR^3
         - 21.924279203556868*cA*cR*I2R*nh - 11.558176191395297*cR^2*I2R*nh  
         + 0.7514027687458106*cR*I2R^2*nh^2 - 62.606296341688555*cA*cR*I2R*nl
         - 45.54081599230199*cR^2*I2R*nl + 9.35837234340779*cR*I2R^2*nh*nl
         + 11.810187596821669*cR*I2R^2*nl^2
```

## Renormalized form factors at three loops

The functions `FormFactorRenNonSing[CurrentType, s]` and `FormFactorRenNhSing[CurrentType, s]` return
the values of finite part in the epsilon expansion for the renormalized and IR-subtracted non-singlet and singlet form factors, respectively.
For the exact definition see Sec. 5 of hep-ph/2207.00027.
The result is the third order correction in the expansion parameter &alpha;<sub>s</sub><sup>(n<sub>l</sub>)</sup>/&pi;,
with the renormalization scale equal to the mass of the quark: &mu;=m.

Example:
```
In[]  := FormFactorRenNonSing[veF1, -1]
Out[] := 3.1071376488041453*cA^2*cR - 3.2341291004185346*cA*cR^2
        + 0.014434738603833732*cR^3 + 0.0435080505064176*cA*cR*I2R*nh
        - 0.0640417696977205*cR^2*I2R*nh - 0.010760857992362089*cR*I2R^2*nh^2
        - 2.590409600644393*cA*cR*I2R*nl + 1.020318797618304*cR^2*I2R*nl
        + 0.0002825275135451592*cR*I2R^2*nh*nl + 0.4940571062801078*cR*I2R^2*nl^2
```
