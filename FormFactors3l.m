(* ::Package:: *)

BeginPackage["FormFactors3l`"];


FormFactorBareNonSing::usage=
"FormFactorBareNonSing[CurrentType, EpsilonOrder,  shat] compute the non-singlet contribution 
 to the bare form factors at third order for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s^(nl + nh)(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorBareNonSing::currenterr=
"The current `1` is unknown";
$FormFactorBareNonSingFailed::usage=
"$FormFactorBareNonSingFailed is returned in case an error occurs.";


FormFactorBareNhSing::usage=
"FormFactorBareSing[CurrentType, EpsilonOrder,  shat] compute the nh-singlet contribution
 for the bare form factor at third order for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s^(nl + nh)(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorBareNhSing::currenterr=
"The current `1` is unknown";
$FormFactorBareNhSingFailed::usage=
"$FormFactorBareNhSingFailed is returned in case an error occurs.";
FormFactorBareNhSing::noaxialsinglet=
"Warining: the singlet contibutions to the axial-vector current form factors are currently not implemented."


veF1::usage=
"The symbol veF1 is used to identify the form factor F1 of the vector current.";
veF2::usage=
"The symbol veF2 is used to identify the form factor F2 of the vector current.";
axF1::usage=
"The symbol axF1 is used to identify the form factor F1 of the axial-vector current.";
axF2::usage=
"The symbol axF2 is used to identify the form factor F2 of the axial-vector current.";
scF1::usage=
"The symbol scF1 is used to identify the form factor F1 of the scalar current.";
psF1::usage=
"The symbol psF1 is used to identify the form factor F1 of the pseudo-scalar current.";


FormFactorRenNonSing::usage=
"FormFactorRenNonSing[CurrentType, shat] compute the finite part
 of the non-singlet renormalized and IR subtracted form factor at third order
 for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s(nl)(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorRenNonSing::currenterr=
"The current `1` is unknown";
$FormFactorRenNonSingFailed::usage=
"$FormFactorRenNonSingFailed is returned in case an error occurs.";


FormFactorRenNhSing::usage=
"FormFactorRenNonSing[CurrentType, shat] compute the finite part
 of the nh-singlet renormalized and IR subtracted form factor at third order
 for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s(nl)(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorRenNhSing::currenterr=
"The current `1` is unknown";
$FormFactorRenNhSingFailed::usage=
"$FormFactorRenNhSingFailed is returned in case an error occurs.";
FormFactorRenNhSing::noaxialsinglet=
"Warining: the singlet contibutions to the axial-vector current form factors are currently not implemented."

cR::usage=
"The SU(N) color factor cR = (N^2-1)/(2N).";
cA::usage=
"The SU(N) color factor cA = N.";
I2R::usage=
"The SU(N) color factor I2R = 1/2.";
nh::usage=
"The flag to denote close loops with massive fermions.";
nl::usage=
"The flag to denote close loops with massless fermions.";
nc::usage=
"The number of colors.";
d33::usage=
"The SU(N) color factor d33[V,V] = (N^2-1)(N^2-4)/(16 N)."

$FormFactors3lVersion = "FormFactors3l 1.0 (29 Jun 2022)"

$FormFactors3lDirectory=DirectoryName[$InputFileName];

Print[""];
Print[$FormFactors3lVersion];
Print["by M. Fael, F. Lange, K. Sch\[ODoubleDot]nwald and M. Steinhauser"];
Print["Massive Vector Form Factors to Three Loops"];  
Print["Phys.Rev.Lett. 128 (2022) 17, 172003"]; 
Print["hep-ph/2202.05276\n"]

Print["Singlet and non-singlet three-loop massive form factors"];
Print["hep-ph/2207.00027\n"];
Print["The functions FormFactorBareNonSing, FormFactorBareNhSing return"];
Print["the values of the bare three-loop form factors"];
Print["for the non-singlet and nh-singlet contributions, respectively."];
Print[""];
Print["The functions FormFactorRenNonSing and FormFactorRenNhSing return"];
Print["the values of the renormalized and IR-subtracted three-loop form factors"];
Print["for the non-singlet and nh-singlet contributions, respectively."];
Print["See the definition in Sec. 4 of hep-ph/2207.00027."];
Print["For the syntax, use e.g. \"?FormFactorBareNonSing\".\n"];


Begin["`Private`"];


(* ::Subsection::Closed:: *)
(*Load Grids*)


Print["Loading grids ..."];


$FormFactors3lDirectory=DirectoryName[$InputFileName];


colorfactors={cR^3,
 cA^2 cR,
 cA cR^2,
 cR^2 I2R nh,
 cR^2 I2R nl,
 cA cR I2R nh,
 cA cR I2R nl,
 cR I2R^2 nl^2,
 cR I2R^2 nh^2,
 cR I2R^2 nl nh,
 nh d33/nc};
colorfactorsstrings=StringReplace[ToString[InputForm[#]]&/@colorfactors[[1;;10]],{"*Qt"->"","*"->"-","^"->""}];
colorfactorsstrings=Join[colorfactorsstrings,{"nh-d33"}];
formfactors={veF1,veF2,axF1,axF2,scF1,psF1};


Do[
    filename = FileNameJoin[{$FormFactors3lDirectory,"datagrid/grid_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
	numgrid[formfactors[[nFF]],colorfactors[[i]],eporder] = Import[filename];
	filename =.;
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,-3,0}];


Do[
	If[nFF===3||nFF===4, Continue[];];
    filename = FileNameJoin[{$FormFactors3lDirectory,"datagrid/grid-sing_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
	numgridsing[formfactors[[nFF]],colorfactors[[i]],eporder] = Import[filename];
	filename =.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,-3,0}];


Do[
    filename = FileNameJoin[{$FormFactors3lDirectory,"datagrid/gridRen_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
	numgridren[formfactors[[nFF]],colorfactors[[i]],eporder] = Import[filename];
	filename =.;
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,0,0}];


Do[
    If[nFF<5, Continue[];];
    filename = FileNameJoin[{$FormFactors3lDirectory,"datagrid/gridRen-sing_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
	numgridsingren[formfactors[[nFF]],colorfactors[[i]],eporder] = Import[filename];
	filename =.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,0,0}];


(* ::Subsection:: *)
(*Load Series at infinity*)


Print["Loading series ..."];


Do[
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-inf_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriesinf[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,-3,0}];


Do[
 If[nFF===3||nFF===4,seriessinginf[formfactors[[nFF]],colorfactors[[i]],eporder]=0; Continue[];];
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-sing-inf_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriessinginf[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,-3,0}];


 Do[
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-ren-inf_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriesreninf[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,0,0}];


Do[
 If[nFF<5, seriesctsinginf[formfactors[[nFF]],colorfactors[[i]],eporder] = 0; Continue[];];
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-sing-ct-inf_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriesctsinginf[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,0,0}];


(* ::Subsection::Closed:: *)
(*Load Series at s=0*)


Do[
 If[nFF===3||nFF===4,seriessing0[formfactors[[nFF]],colorfactors[[i]],eporder]=0; Continue[];];
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-sing-0_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriessing0[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,-3,0}];


Do[
 If[nFF<5, seriesctsing0[formfactors[[nFF]],colorfactors[[i]],eporder] = 0; Continue[];];
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-sing-ct-0_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriesctsing0[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,0,0}];


(* ::Subsection::Closed:: *)
(*Load Series at s=4*)


Do[
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-4_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 series4[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,-3,0}];


Do[
 If[nFF===3||nFF===4,seriessing4[formfactors[[nFF]],colorfactors[[i]],eporder]=0; Continue[];];
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-sing-4_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriessing4[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,-3,0}];


Do[
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-ct-4_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriesct4[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,0,0}];


Do[
 If[nFF<5, seriesctsing4[formfactors[[nFF]],colorfactors[[i]],eporder] = 0; Continue[];];
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-sing-ct-4_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriesctsing4[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,0,0}];


(* ::Subsection::Closed:: *)
(*Load Series at s=16*)


Do[
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-16_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 series16[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,-3,0}];


Do[
 If[nFF===3||nFF===4,seriessing16[formfactors[[nFF]],colorfactors[[i]],eporder]=0; Continue[];];
 filename=FileNameJoin[{$FormFactors3lDirectory,"expansions/series-sing-16_"<>ToString[formfactors[[nFF]]]<>"_"<>colorfactorsstrings[[i]]<>"_ep"<>ToString[eporder]<>".m.gz"}];
 seriessing16[formfactors[[nFF]],colorfactors[[i]],eporder] =  Import[filename];
 filename=.;
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,-3,0}];


Print["Loading grids and series expansions: DONE!"];


(* ::Subsection::Closed:: *)
(*Prepare Interpolating Functions*)


(* Interpolation for the Bare Form Factors - non singlet *)
Do[
	grid = Select[numgrid[formfactors[[nFF]],colorfactors[[i]],eporder], #[[1]] < 4&];
	interpolationfunInfTo4[formfactors[[nFF]],colorfactors[[i]],eporder]= Interpolation[grid,InterpolationOrder->5];
	grid = Select[numgrid[formfactors[[nFF]],colorfactors[[i]],eporder], 4 < #[[1]] < 16&];
	interpolationfun4To16[formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
	grid = Select[numgrid[formfactors[[nFF]],colorfactors[[i]],eporder], 16 < #[[1]] &];
	interpolationfun16ToInf[formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,-3,0}];
grid=.;


(* Interpolation for the Bare Form Factors - singlet *)
Do[
    If[nFF===3||nFF===4,
      interpolationfunsingInfTo0[formfactors[[nFF]],colorfactors[[i]],eporder][s_]  = 0; 
      interpolationfunsing0To4[formfactors[[nFF]],colorfactors[[i]],eporder][s_]    = 0;
      interpolationfunsing4To16[ formfactors[[nFF]],colorfactors[[i]],eporder][s_]  = 0;
      interpolationfunsing16ToInf[formfactors[[nFF]],colorfactors[[i]],eporder][s_] = 0;
      Continue[];];
	grid = Select[numgridsing[formfactors[[nFF]],colorfactors[[i]],eporder], #[[1]] <= 0 &];
	interpolationfunsingInfTo0[formfactors[[nFF]],colorfactors[[i]],eporder]= Interpolation[grid,InterpolationOrder->5];
	grid = Select[numgridsing[formfactors[[nFF]],colorfactors[[i]],eporder], 0 < #[[1]] < 4&];
	interpolationfunsing0To4[formfactors[[nFF]],colorfactors[[i]],eporder]= Interpolation[grid,InterpolationOrder->5];
	grid = Select[numgridsing[ formfactors[[nFF]],colorfactors[[i]],eporder], 4 < #[[1]] < 16&];
	interpolationfunsing4To16[ formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
	grid = Select[numgridsing[ formfactors[[nFF]],colorfactors[[i]],eporder], 16 < #[[1]] &];
	interpolationfunsing16ToInf[formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,-3,0}];
grid=.;


(* Interpolation for the Renormalized and IR-subtracted Form Factors - Non Singlet*)
Do[
	grid = Select[numgridren[formfactors[[nFF]],colorfactors[[i]],eporder],#[[1]] < 4&];
	interpolationfunrenInfTo4[formfactors[[nFF]],colorfactors[[i]],eporder]= Interpolation[grid,InterpolationOrder->5];
	grid = Select[numgridren[formfactors[[nFF]],colorfactors[[i]],eporder], 4 < #[[1]] < 16&];
	interpolationfunren4To16[formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
	grid = Select[numgridren[formfactors[[nFF]],colorfactors[[i]],eporder], 16 < #[[1]] &];
	interpolationfunren16ToInf[formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
,{i,1,Length[colorfactors]-1},{nFF,1,Length[formfactors]},{eporder,0,0}];
grid=.;


 (* Interpolation for the Renormalized and IR-subtracted Form Factors - Singlet *)
 (* veF1 and veF2 are already finite *)
Do[
    If[nFF===3||nFF===4,
      interpolationfunsingrenInfTo0[formfactors[[nFF]],colorfactors[[i]],eporder][s_]  = 0; 
      interpolationfunsingren0To4[formfactors[[nFF]],colorfactors[[i]],eporder][s_]    = 0;
      interpolationfunsingren4To16[ formfactors[[nFF]],colorfactors[[i]],eporder][s_]  = 0;
      interpolationfunsingren16ToInf[formfactors[[nFF]],colorfactors[[i]],eporder][s_] = 0;
      Continue[];];
    If[nFF===1||nFF===2, 
		grid = Select[numgridsing[formfactors[[nFF]],colorfactors[[i]],eporder], #[[1]] <= 0 &];
		interpolationfunsingrenInfTo0[formfactors[[nFF]],colorfactors[[i]],eporder]= Interpolation[grid,InterpolationOrder->5];
		grid = Select[numgridsing[formfactors[[nFF]],colorfactors[[i]],eporder], 0 < #[[1]] < 4&];
		interpolationfunsingren0To4[formfactors[[nFF]],colorfactors[[i]],eporder]= Interpolation[grid,InterpolationOrder->5];
		grid = Select[numgridsing[ formfactors[[nFF]],colorfactors[[i]],eporder], 4 < #[[1]] < 16&];
		interpolationfunsingren4To16[ formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
		grid = Select[numgridsing[ formfactors[[nFF]],colorfactors[[i]],eporder], 16 < #[[1]] &];
		interpolationfunsingren16ToInf[formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
		];
	If[nFF===5||nFF===6, 
		grid = Select[numgridsingren[formfactors[[nFF]],colorfactors[[i]],eporder], #[[1]] <= 0&];
		interpolationfunsingrenInfTo0[formfactors[[nFF]],colorfactors[[i]],eporder]= Interpolation[grid,InterpolationOrder->5];
		grid = Select[numgridsingren[formfactors[[nFF]],colorfactors[[i]],eporder], 0 < #[[1]] < 4&];
		interpolationfunsingren0To4[formfactors[[nFF]],colorfactors[[i]],eporder]= Interpolation[grid,InterpolationOrder->5];
		grid = Select[numgridsingren[ formfactors[[nFF]],colorfactors[[i]],eporder], 4 < #[[1]] < 16&];
		interpolationfunsingren4To16[ formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
		grid = Select[numgridsingren[ formfactors[[nFF]],colorfactors[[i]],eporder], 16 < #[[1]] &];
		interpolationfunsingren16ToInf[formfactors[[nFF]],colorfactors[[i]],eporder] = Interpolation[grid,InterpolationOrder->5];
		];	
		
,{i,1,Length[colorfactors]},{nFF,1,Length[formfactors]},{eporder,0,0}];
grid=.;


(* ::Subsection::Closed:: *)
(*Function for bare form factors*)


FormFactorBareNonSing[CurrentType_Symbol, EpsilonOrder_Integer,  s_?NumericQ]:=
Block[{VariableChangeInverseInf,VariableChangeInverse4,VariableChangeInverse16},
	VariableChangeInverseInf = {xsminf->-(6/(-10+ss))};
	VariableChangeInverse4 = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	VariableChangeInverse16 = {sqrt16ms->Sign[16-ss]Sqrt[16-ss]};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorBareNonSing::currenterr,CurrentType]; Return[$FormFactorBareFailed];];
	If[EpsilonOrder > 0 || EpsilonOrder < -3,Return[$FormFactorBareFailed];];
	Which[
	-40  <=s<= 4-1/4,   Sum[colorfactors[[i]] interpolationfunInfTo4[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]-1}],
	 4+1/4 <=s<= 31/2,  Sum[colorfactors[[i]] interpolationfun4To16[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]-1}],
	33/2 <=s<= 60,	    Sum[colorfactors[[i]] interpolationfun16ToInf[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]-1}],
        s<-40 || s> 60,     Sum[colorfactors[[j]] seriesinf[CurrentType,colorfactors[[j]],EpsilonOrder],{j,1,Length[colorfactors]-1}]/.VariableChangeInverseInf/.ss->s,
	4-1/4  <s< 4+1/4,   Sum[colorfactors[[j]] series4[CurrentType,colorfactors[[j]]  ,EpsilonOrder],{j,1,Length[colorfactors]-1}]/.VariableChangeInverse4/.ss->s,
	31/2 <s< 33/2,      Sum[colorfactors[[j]] series16[CurrentType,colorfactors[[j]]  ,EpsilonOrder],{j,1,Length[colorfactors]-1}]/.VariableChangeInverse16/.ss->s,
	True,$FormFactorBareNonSingFailed]/. 0. -> 0
];


FormFactorBareNhSing[CurrentType_Symbol, EpsilonOrder_Integer,  s_?NumericQ]:=
Block[{VariableChangeInverse0,VariableChangeInverse4,VariableChangeInverse16,VariableChangeInverseInfSing},
    VariableChangeInverse0 = {sqrtms->Sign[-ss]Sqrt[-ss]};
	VariableChangeInverseInfSing = {xsminf->-(8/(-8+ss))};
	VariableChangeInverse4 = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	VariableChangeInverse16 = {sqrt16ms->Sign[16-ss]Sqrt[16-ss]};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorBareNhSing::currenterr,CurrentType]; Return[$FormFactorBareFailed];];
	If[CurrentType===3||CurrentType===4, Message[FormFactorBareNhSing::noaxialsinglet];];
	If[EpsilonOrder > 0 || EpsilonOrder < -3,Return[$FormFactorBareFailed];];
	Which[
	-40  <=s<=-1/2,      Sum[colorfactors[[i]] interpolationfunsingInfTo0[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]}],
	1/4  <=s<= 4-1/4,    Sum[colorfactors[[i]] interpolationfunsing0To4[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]}],       
	4+1/4  <=s<= 31/2,   Sum[colorfactors[[i]] interpolationfunsing4To16[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]}],
        33/2 <=s<= 60,       Sum[colorfactors[[i]] interpolationfunsing16ToInf[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]}],
        -1/2 <s< 1/4,        Sum[colorfactors[[j]] seriessing0[CurrentType,colorfactors[[j]]  ,EpsilonOrder],{j,1,Length[colorfactors]}]/.VariableChangeInverse0/.ss->s,
	s<-40 || s>60,       Sum[colorfactors[[j]] seriessinginf[CurrentType,colorfactors[[j]],EpsilonOrder],{j,1,Length[colorfactors]}]/.VariableChangeInverseInfSing/.ss->s,
	4-1/4  <s< 4+1/4,    Sum[colorfactors[[j]] seriessing4[CurrentType,colorfactors[[j]]  ,EpsilonOrder],{j,1,Length[colorfactors]}]/.VariableChangeInverse4/.ss->s,
	31/2 <s< 33/2,       Sum[colorfactors[[j]] seriessing16[CurrentType,colorfactors[[j]]  ,EpsilonOrder],{j,1,Length[colorfactors]}]/.VariableChangeInverse16/.ss->s,
	True,$FormFactorBareNhSingFailed]/. 0. -> 0
];


(* ::Subsection:: *)
(*Function Renormalized Form Factors*)


FormFactorRenNonSing[CurrentType_Symbol, s_?NumericQ]:=
Block[{VariableChangeInverseInf,VariableChangeInverse4,VariableChangeInverse16,EpsilonOrder,repx2s},
	EpsilonOrder = 0;
	VariableChangeInverseInf = {xsminf->-(6/(-10+ss))};
	VariableChangeInverse4  = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	repx2s = {x -> (2 + Sqrt[-4 + ss]*Sqrt[ss] - ss)/2};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorRenNonSing::currenterr,CurrentType]; Return[$FormFactorRenNonSingFailed];];
	Which[
	-40 <= s <= 4-1/4,  Sum[colorfactors[[i]] interpolationfunrenInfTo4[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]-1}],
	4+1/4 <= s < 16,    Sum[colorfactors[[i]] interpolationfunren4To16[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]-1}],
        16  < s <= 60,      Sum[colorfactors[[i]] interpolationfunren16ToInf[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]-1}],
	s<-40 || s>60,     Sum[colorfactors[[j]] seriesreninf[CurrentType,colorfactors[[j]],EpsilonOrder]/.VariableChangeInverseInf/.ss->s,{j,1,Length[colorfactors]-1}],
	4-1/4 < s < 4+1/4, Sum[colorfactors[[j]] 
	                   Expand[ (series4[CurrentType,colorfactors[[j]]  ,EpsilonOrder]/.VariableChangeInverse4/.ss->s)
	                          +(seriesct4[CurrentType,colorfactors[[j]]  ,EpsilonOrder]/.z->1+x/.repx2s/.ss->s)]
	                          ,{j,1,Length[colorfactors]-1}]//#&,
	True,$FormFactorRenNonSingFailed]/. 0. -> 0
]; 


FormFactorRenNhSing[CurrentType_Symbol,  s_?NumericQ]:=
Block[{VariableChangeInverse0,VariableChangeInverse4,VariableChangeInverse16,VariableChangeInverseInfSing,EpsilonOrder,repx2s},
	EpsilonOrder = 0;
	VariableChangeInverse0 = {sqrtms->Sign[-ss]Sqrt[-ss]};
	VariableChangeInverseInfSing = {xsminf->-(8/(-8+ss))};
	VariableChangeInverse4 = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	VariableChangeInverse16 = {sqrt16ms->Sign[16-ss]Sqrt[16-ss]};
	repx2s = {x -> (2 + Sqrt[-4 + ss]*Sqrt[ss] - ss)/2};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorRenNhSing::currenterr,CurrentType]; Return[$FormFactorRenNhSingFailed];];
	If[CurrentType===3||CurrentType===4, Message[FormFactorRenNhSing::noaxialsinglet];];
	Which[
	-40  <=s<=-1/2,     Sum[colorfactors[[i]] interpolationfunsingrenInfTo0[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]}],
	1/4  <=s<= 4-1/4,   Sum[colorfactors[[i]] interpolationfunsingren0To4[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]}],       
	4+1/4  <=s < 16,    Sum[colorfactors[[i]] interpolationfunsingren4To16[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]}],
        16 < s <= 60,       Sum[colorfactors[[i]] interpolationfunsingren16ToInf[CurrentType,colorfactors[[i]],EpsilonOrder][s],{i,1,Length[colorfactors]}],
	s<-40 || s>60,      Sum[colorfactors[[j]] 
	                       Expand[ (  seriessinginf[CurrentType,colorfactors[[j]],EpsilonOrder]/.VariableChangeInverseInfSing/.ss->s) 
	                              +(seriesctsinginf[CurrentType,colorfactors[[j]],EpsilonOrder]/.repx2s/.ss->s)]
	                           ,{j,1,Length[colorfactors]}],
	-1/2 < s < 1/4,     Sum[colorfactors[[j]] 
	                       Expand[ (  seriessing0[CurrentType,colorfactors[[j]],EpsilonOrder]/.VariableChangeInverse0/.ss->s) 
	                              +(seriesctsing0[CurrentType,colorfactors[[j]],EpsilonOrder]/.z->1-x/.repx2s/.ss->s)]
	                           ,{j,1,Length[colorfactors]}],
        4-1/4 < s < 4+1/4,  Sum[colorfactors[[j]] 
	                       Expand[ (  seriessing4[CurrentType,colorfactors[[j]],EpsilonOrder]/.VariableChangeInverse4/.ss->s) 
	                              +(seriesctsing4[CurrentType,colorfactors[[j]],EpsilonOrder]/.z->1+x/.repx2s/.ss->s)]
	                           ,{j,1,Length[colorfactors]}],
	True,$FormFactorRenNhSingFailed]/. 0. -> 0
];


(* ::Subsection::Closed:: *)
(*Epilogue*)


End[];


EndPackage[];
