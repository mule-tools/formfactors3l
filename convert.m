BuildX[{x0_Real, d_Real, n_Integer}] := x0 + d Range[0, n]
BuildX[{A__List}] := Join @@ BuildX /@ {A}

xspec1 = {
 {-41.00001, 0.05, 620},
 {-9.99001, 0.01, 1198},
 {1.99999, 0.01, 190},
 {4.09999, 0.01, 1590},
 {20.04999, 0.05, 819}
};
xspec2 = {
 {-41.00001, 0.05, 620},
 {-9.99001, 0.01, 1198},
 {2.00999, 0.01, 189},
 {4.09999, 0.01, 1590},
 {20.04999, 0.05, 819}
};

(* FIXME *)
GetXSpec[x_] := If[Length[BuildX[xspec1]]==Length[x], xspec1, xspec2]

ExportFortran[dat_, xspec_, stream_OutputStream] := (
  BinaryWrite[stream, 4, "UnsignedInteger32"];
  BinaryWrite[stream, Length[xspec], "UnsignedInteger32"];
  BinaryWrite[stream, 4, "UnsignedInteger32"];

  BinaryWrite[stream, 8*2*Length[xspec], "UnsignedInteger32"];
  BinaryWrite[stream, xspec[[;;,1;;2]], "Real64"];
  BinaryWrite[stream, 8*2*Length[xspec], "UnsignedInteger32"];

  BinaryWrite[stream, 4*Length[xspec], "UnsignedInteger32"];
  BinaryWrite[stream, xspec[[;;,3]], "UnsignedInteger32"];
  BinaryWrite[stream, 4*Length[xspec], "UnsignedInteger32"];

  BinaryWrite[stream, 8*2*Length[dat], "UnsignedInteger32"];
  BinaryWrite[stream, dat, "Real64"];
  BinaryWrite[stream, 8*2*Length[dat], "UnsignedInteger32"];
)

ExportFortran[dat_, xspec_, fn_String] := Module[{stream},
  stream = OpenWrite[fn, BinaryFormat->True];
  ExportFortran[dat, xspec, stream];
  Close[stream];
]

ExportFortran[file_] := Module[{dat},
  dat = Import[file];

  ExportFortran[
    ReIm[dat[[;;,2]]],
    GetXSpec[dat[[;;,1]]],
    StringReplace[file,".m.gz"->".bin"]
  ]
];


ExportFortran /@ FileNames["datagrid/*.m.gz"];
